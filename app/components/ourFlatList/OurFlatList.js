import React, {Component} from 'react';
import {View, StyleSheet, Text, ScrollView, Image} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const DATA = [
  {
    id:'1',
    title: 'Proteina pura concentrada',
    img:
      'https://http2.mlstatic.com/nitro-whey-3kg-shaker-regalo-delivery-gratis-D_NQ_NP_280021-MPE20698493331_052016-F.jpg',
    descripcion:
      'Es un suero de leche concetrado, producto nacional, ayuda a definir, tambien en etapa de volumen limpio, si quieres ser fuerte se NITRO-WHEY a tan solo 200s/.',
  },
  {
    id:'2',
    title: 'Proteina pura isolatada',
    img:
      'https://http2.mlstatic.com/iso-whey-90-proteina-12-kg-promociones-D_NQ_NP_829356-MPE31254451073_062019-F.jpg',
    descripcion:
      'Es un suero de lecho isolatada, producto nacional que mejora la digestion proteica, ayuda a la tonificacion y definicion muscular, no contiene lactosa ,colesterol ni grasa a tan salo 300s/.',
  },
  {
    id:'3',
    title: 'Proteina pura de carne',
    img:
      'https://intermediary-i.linio.com/p/1e1e0990cf7c045744453e6bfa098e82-product.jpg',
    descripcion:
      'Proteina a base carne isolatada, producto nacional, alimenta los musculos por mas tiempo, ayuda a la tonificacion y definicion muscular , no contiene lactosa, colesterol ni grasa a tan solo 340s/.',
  },
  {
    id:'4',
    title: 'Proteina pura a base de Soja',
    img:
      'https://intermediary-i.linio.com/p/0f6b1ce9a3a7e1532791282d4dbd23de-product.jpg',
    descripcion:
      'Batido de dieta a base de proteina isolatada de soja,producto nacional, ayuda a manejar la ingesta proteica vegetal, mejora la digestion y el transito intestinal a tan solo 135s/.',
  },  
  {
    id:'5',
    title: 'Quemadores variados',
    img:
      'https://quemadoresdegrasas.com/wp-content/uploads/2013/02/quemadores-de-grasa-tipos.jpg',
    descripcion:
      'Formula base para yudar a la perdida de grasa con garcinia cambogia y sin cafeina, con carnitinina que moviliza y ayuda a quemar la grasa ademas de cafeina para darte mas energia a tan solo 50s/.',
  }, 
  {
    id:'6',
    title: 'Proteina pura de carne',
    img:
      'https://http2.mlstatic.com/beef-protein-universe-nutrition-proteina-de-carne-12kg-D_NQ_NP_764888-MPE31254890105_062019-F.jpg',
    descripcion:
      'Proteina a base de carne, isolatada de produccion nacional que alimenta los musculos por mas tiempo, ayuda a la tonificacion y definicion muscular, no contiene lactosa, colesterol ni grasa a tan solo 340s/.',
  }, 
  {
    id:'7',
    title: 'Proteina de dieta',
    img:
      'https://http2.mlstatic.com/protein-diet-15-kg-recojo-en-jesus-maria-D_NQ_NP_632325-MPE25414802620_032017-F.jpg',
    descripcion:
      'Batido de dieta a base de suero de leche con algas marinas, garcinia cambogia y supresores naturales del apetito de produccion nacional que ayuda a la perdida de peso a tan solo 190s/.',
  }, 
  {
    id:'8',
    title: 'Ganador de masa de carne',
    img:
      'https://http2.mlstatic.com/beef-mass-2kilos-shaker-de-regalo-envio-gratis-D_NQ_NP_778836-MPE27921640469_082018-F.jpg',
    descripcion:
      'Ganador de masa muscular a base de proteina de carne de produccion nacional que mejora la digestion proteica con un aumento rapido de masa muscular que ademas al ser a base de carne no contiene lactosa ni colesterol a tan solo 200s/.',
  }, 
];

function Item({title,image,descripcion}) {
  return (
    <View style={styles.item}>
      <Image style={styles.image} source={{uri: image}}/>
      <View style={styles.column}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.Descripcion}>{descripcion}</Text>
      </View>
      
    </View>
  );
}

export default class OurFlatList extends Component {
  constructor(props) {
    super(props);
  }

  render(){
    return (
      <View style={styles.container}>
        {}
        <ScrollView>
          {DATA.map(item => (
            <TouchableOpacity style={styles.item} onPress={()=> this.props.navigation.navigate('Details',{
              key: item.id,
              itemID: item.id,
              itemTitle: item.title,
              itemDesc: item.descripcion,
              itemPic: item.img})}>
              
              <Item title={item.title} image={item.img} descripcion={item.descripcion}/>
              
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
    );

  }
  
}

const styles = StyleSheet.create({
  container: {
    marginTop: 15,
    backgroundColor:'#999999',
  },
  item: {
    flexDirection: 'row',
    backgroundColor: 'gray',
    padding: 1,
    marginVertical: 5,
    marginHorizontal: 5
  },
  column: {
    flexDirection: 'column',
    backgroundColor: 'gray',
    padding: 1,
    marginLeft: 10,
    marginRight: 210,
    marginHorizontal:10,
    marginVertical: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 25,
  },
  Descripcion: {
    fontSize: 12,
    color: 'red',
  },
});
